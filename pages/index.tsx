import Head from 'next/head';
import styles from '../styles/Home.module.css';
import React from 'react';
import Header from '../components/ui/Header';
import Button from '@mui/material/Button';

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1 className={styles.title}>
          Welcome to <a href="https://nextjs.org">Next.js!</a>
        </h1>

        <p className={styles.description}>
          Get started by editing <code>pages/index.js</code>
        </p>

        <div>
          <Button variant="contained">Hello World</Button>
        </div>

        <div className={styles.grid}>
          <a>
            <Header />
          </a>

          <a>
            <Header />
          </a>
        </div>
      </main>

      <footer style={{ backgroundColor: 'red' }}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
